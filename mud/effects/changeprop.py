# -*- coding: utf-8 -*-
# Copyright (C) 2014 Denys Duchier, IUT d'Orléans
#==============================================================================

from .effect import Effect2, Effect3
from mud.events import ChangePropEvent, ChangePropEvent2

class ChangePropEffect(Effect2):
    EVENT = ChangePropEvent

    def make_event(self):
        return self.EVENT(self.actor, self.object, self.yaml["modifs"])

class ChangePropEffect2(Effect3):
    EVENT = ChangePropEvent2

    def make_event(self):
        return self.EVENT(self.actor, self.object, self.object2, self.yaml["modifs"])

