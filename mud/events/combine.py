# -*- coding: utf-8 -*-
# Copyright (C) 2015 Kevin Robin, IUT d'Orléans
#==============================================================================

from .event import Event1, Event3
import mud.game

class CombineEvent(Event3):
    NAME = "combine"
    def perform(self):
        res = self.find_object_by_id(getattr(self, 'res'))

        if self.object not in self.actor:
            self.fail()
            self.inform('precombine.failed')
            return
        elif self.object2 not in self.actor:
            self.fail()
            self.inform('precombine.failed')
            return

        self.actor.remove(self.object)            
        self.actor.remove(self.object2)
        self.actor.add(res)
        self.result = res
        self.inform('combine')
        
    def find_object_by_id(self, i):
        world = mud.game.GAME.world
        return world[i]        

class PreCombineEvent(Event3):
    NAME = "precombine"

    def perform(self):
        if self.object not in self.actor:
            self.fail()
            return self.combine_failure()
        if self.object2 not in self.actor:
            self.fail()
            return self.combine_failure()
        if not self.object.has_prop("combinable-with") and not self.object2.has_prop("combinable-with"):
            self.fail()
            return self.combine_failure()
        self.inform("precombine")

    def combine_failure(self):
        self.fail()
        self.inform("precombine.failed")