# -*- coding: utf-8 -*-
# Copyright (C) 2015 Mathieu HIREL, IUT d'Orléans
#==============================================================================

from .event import Event1, Event2
import mud.game

class TaxiApproachEvent(Event1):
  NAME = "taxi-approach"

  def perform(self):
    location = self.actor.container()
    if location.has_prop('taxi-here'):
      self.fail()
      self.inform('taxi-approach.failed')
      return
    location.add_prop('taxi-here')
    self.inform('taxi-approach')


class PullEvent(Event2):
  NAME = "pull"

  def perform(self):
    if not self.object.has_prop("pullable"):
      self.fail()
      self.inform("pull.failed")
      return
    # if self.actor.container().id == self.object.container().id:
    self.actor.container().add_prop("pullfrom")
    self.inform("pull")

class EnterTaxiEvent(Event1):
  NAME = "enter-taxi"

  def perform(self):
    self.actor.move_to(self.direction)
    self.inform("enter-taxi")

  def buffer_inform(self, dotpath, **kargs):
        text = self.get_template(dotpath, **kargs)
        if text is None:
        	 text = self.actor.container().get_template(dotpath, **kargs)
        if text:
            html = self.format(text, **kargs)
            self.buffer_htmlize(html)

class ExitTaxiEvent(Event1):
	NAME = "exit-taxi"

	def perform(self):
		location = self.find_destination_location()
		if location is None:
			self.fail()
			return
		self.actor.move_to(location)
		self.inform('exit-taxi')


	def find_destination_location(self):
		world = mud.game.GAME.world
		for value in world.values():
			if value.is_location() and value.has_prop('taxi-here'):
				return value
		return None

class GoToEvent(Event2):
  NAME = "go-to"

  def perform(self):

    if not self.object:
      self.fail()
      self.inform('go-to.failed.not-found')
      return

    if len(self.object) > 1:
      self.fail()
      self.inform('go-to.failed.ambiguous')
      return
    location  = self.object[0]
    self.object = location
    print(list(location.get_props()), location.id)
    if self.actor.container().id != "taxi-000": 
      self.fail()
      return
    elif location.has_prop('taxi-here'):
      self.fail()
      self.inform('go-to.failed.already-here')
      return
    elif not location.has_prop('exterieur'):
      self.fail()
      self.inform("go-to.failed.not-exterieur")
      return
      
    self.find_current_taxi_location().remove_prop('taxi-here')
    location.add_prop('taxi-here')
    self.inform("go-to")

  def find_current_taxi_location(self):
    world = mud.game.GAME.world
    for value in world.values():
      if value.is_location() and value.has_prop('taxi-here'):
        return value
    return None
  def get_event_templates(self):
    return self.actor.get_event_templates()
